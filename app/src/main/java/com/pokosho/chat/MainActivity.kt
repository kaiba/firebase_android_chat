package com.pokosho.chat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Thread.sleep
import java.util.*

class Message {
    var message: String = ""
    var lat: Double = 0.0
    var long: Double = 0.0

}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        sendFirestoreTest()
        readFirestoreTest()
    }

    fun readFirestoreTest() {
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection("messages").whereGreaterThan("lat", 35).whereLessThan("lat", 38)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result.forEach { res ->
                    Log.d("chat", res.data.toString())
                    Toast.makeText(this,res.data.toString(), Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    fun sendFirestoreTest() {
        val db = FirebaseFirestore.getInstance()
        val messages = HashMap<String, Any>()
        for (i in 0..9){
            sleep(1000)
            messages.put("message", "現在時刻は" + Date().toString())
            messages.put("lat", 35.6848523 + i)
            messages.put("lon", 139.767602 + i)
            // Add a new document with a generated ID
            //db.collection("messages").document("messages")
            db.collection("messages").add(messages)
                    .addOnSuccessListener {
                        Toast.makeText(this, "OK", Toast.LENGTH_LONG).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
        }
    }
}
